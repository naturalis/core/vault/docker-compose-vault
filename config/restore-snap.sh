#!/bin/bash
# Belongs to restore test vault, fase 2
# Only runs on a virgin HC Vault
set -xv
  
INIT_FILE=/vault/data/init.txt
# Initialize
vault operator init > $INIT_FILE

# Unseal
for i in 1 2 3
do
 KEY=$(head -n $i $INIT_FILE | tail -1 | awk '{print $4}')
 vault operator unseal $KEY
done

$ Unlock
TOKEN=$(head -n 7 $INIT_FILE | tail -1 | awk '{print $4}')
vault login $TOKEN

# Restore
vault operator raft snapshot restore --force /vault/data/raft/raft_snapshot.snap

# Make active
reboot

